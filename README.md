# SSI eIDAS Bridge (SEB) interop' 

## What is SSI eIDAS Bridge

The eIDAS bridge is a component [that we are developing](https://gitlab.grnet.gr/essif-lab/infrastructure/validated-id) to proposes to enhance the legal certainty of any class of verifiable credential, by incorporating the issuer’s advanced or qualified electronic signature (if the issuer is a natural person) or seal (if the issuer is a legal person).

The **main role** of the eIDAS Bridge is to assist:
1. **Issuers**, in the process of signing/sealing a verifiable credential, and 
1. **Verifiers**, in the last mile of the verification process, to help identifying the natural or legal person behind an issuer’s DID.


## Interoperability 

We are coordinating a joint effort to demonstrate interoperability of SSI eIDAS Bridge among different participants in eSSIF-Lab.

The objective is to agree on a common understanding of the technical specifications of the SEB, define interoperability test flows and write test suites. 

These are the teams that are interested in using SEB in their project:

|Team|Project summary|
|-|-|
|off-blocks|Digital ID and signatures for businesses and organisations|
|SSI4DTM|Digital Transaction Management platform to execute any cross-border transactions:NDAs, contracts, bids, etc...
|SICPA|Issuer / Verifier service (DIDComm / CHAPI) and Aries-compatible Wallet with multiple signature formats (AnonCreds and JSON-LD standards) and able to interact with multiple issuers and verifier DID-methods
|ComKYC|A protocol to Issue KYC Verifiable Credentials based on PSD2 Bank Payment Service directives.|
|Gataca|Universal Connect (API-based authentication module for Verifiers with Web-based administration portal)|

Some subgrantees will integrate their own implementation of the SEB tech specs, thus improving the quality and the significance of the interoperability tests.

| Team                            | Infra / Business | Company      | Acronym (link to Gitlab) | Implement own bridge | Interop VCs + wallet | Integrate bridge | Integrate Bridge at Issuer level | Integrate Bridge at Verifier level | Integrate Bridge at wallet level | Proof Format | Signature Type / Suite                         | DID Methods         | DID Resolution           | Other interop specs/focus        |
|---------------------------------|------------------|--------------|--------------------------|----------------------|----------------------|------------------|----------------------------------|------------------------------------|----------------------------------|--------------|------------------------------------------------|---------------------|--------------------------|----------------------------------|
| Off-Blocks                      | Business         | Off-Blocks   | OBDID                    | √                    | √                    | -                | -                                | -                                  | -                                | LD-Proofs    | Ed25519 Signature 2018                         | did:factom          | Universal Resolver       |                                  |
| SSI4DTM                         | Business         | Joinyourbit  | SSI4DTM                  | √                    | √                    | √                |                                  |                                    |                                  | LD-Proofs    | Ed25519 Signature 2018/RSA Signature 2018/RSA* |                     | Universal Resolver       |                                  |
| SICPA                           | Infra            | SICPA        |                          | -                    | √                    | √                |                                  |                                    |                                  | LD-Proofs    | Ed25519 Signature 2018                         | did:sovrin, did:key | Universal Resolver       |                                  |
| validated ID                    | Infra            | validated ID | SEB                      | √                    |                      |                  |                                  |                                    |                                  | LD-Proofs    | RSA Signature 2018                             |                     |                          |                                  |
| Gaya                            | Business         | NYM Srl      |                          | no                   | √                    | √                | √                                | √                                  |                                  | LD-Proofs    | RSA Signature 2018                             | did:com             | Universal Resolver       |                                  |
| Verifiable Credential Authority | Infrastructure   | NYM Srl      | VCA                      | no                   | √                    | √                | √                                | √                                  |                                  | LD-Proofs    | RSA Signature 2018                             | did:com             | Universal Resolver       |                                  |
| Gataca                          | Business         | Gataca       | GATC                     | -                    | √                    | √                | √                                | √                                  | √                                | LD-Proofs    | Ed25519 Signature 2018                         | did:gatc            | Universal Resolver (WIP) | DID Resolution<br/>Verifier APIS |

## Interoperability tests

The aim of these tests is to validate different implementations of the SEB technical specification by using the SEB [test suites](https://gitlab.grnet.gr/essif-lab/interoperability/ssi-eidas-bridge/-/tree/master/eIDAS%20Bridge%20test%20suite).

> TBD

## Integration tests

Besides interoperability tests, we plan to do integration tests where participants can use the eIDAS Bridge from in Issuer or Verifier eSSIF-Lab's SSI roles.
