# <!--subproject name--> SSI4DTM SSI eIDAS BRIDGE

For more information, please contact: projects@joinyourbit.com


## SEB API

The following swagger describes the structure of the eSSIF-lab eSeal API for SEB.

API release 0.1:

- CAdES signature using Joinyourbit eIDAS Qualified Seal
- RSA PKCS#1 (raw) signature using Joinyourbit eIDAS Qualified Seal
- CAdES velidation (release 0.2)
- CRSA PKCS#1 (raw) validation (release 0.2)

The APIs will be available for testing from Friday 29th January.

For test purpose use the following signed token: **V17shxPtLr**

Below, a VC example to sign with SEB:

`{
  "token": "V17shxPtLr",
  "verifiableCredential": 
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1"
  ],
  "id": "http://example.edu/credentials/3732",
  "type": ["VerifiableCredential", "PersonalIdCredential"],
  "issuer": "did:com:76e12ec712ebc6f1c221ebfeb1f",
  "issuanceDate": "2021-01-01T19:23:24Z",
  "credentialSubject": {
    "id": "did:com:ebfeb1f712ebc6f1c276e12ec21",
    "personaldata": {
      "name": "Mario",
      "surname": "Rossi",
	  "gender": "M",
	  "bithdate": "1971-01-01",
	  "countryofbirth": "Italy",
	  "provinceofbirth": "RM",
	  "cityofbirth": "Rome",
	  "typeofdocument": "Identity Card",
	  "documentnumber": "123456567","countryofresidence": "Italy",
	  "provinceofresidence": "RM",
	  "cityofresidence": "Rome",
	  "address": "via Leonardo da Vinci, 1",
	  "zipcode": "00100"
	}
  },
  "evidence": [{
    "id": "https://example.edu/evidence/f2aeec97-fc0d-42bf-8ca7-0548192d4231",
    "type": ["DocumentVerification"],
    "verifier": "https://example.edu/issuers/14",
    "evidenceDocument": "Passport",
    "subjectPresence": "Digital",
    "documentPresence": "Digital"
  },
  {
    "id": "https://example.edu/evidence/f2aeec97-fc0d-42bf-8ca7-0548192d4231",
    "type": ["VideoIdentification"],
    "verifier": "https://example.edu/issuers/14",
    "evidenceDocument": "ARUBATOKEN",
    "subjectPresence": "Digital",
    "documentPresence": "Digital"
  }],
  "proof": [{
    "type": "RsaSignature2018",
    "created": "2021-01-15T21:19:10Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "https://example.com/jdoe/keys/1",
    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19DJBMvvFAIC00nSGB6Tn0XKbbF9XrsaJZREWvR2aONYTQQxnyXirtXnlewJMBBn2h9hfcGZrvnC1b6PgWmukzFJ1IiH1dWgnDIS81BH-IxXnPkbuYDeySorc4QU9MJxdVkY5EL4HYbcIfwKj6X4LBQ2_ZHZIu1jdqLcRZqHcsDF5KKylKc1THn5VRWy5WhYg_gBnyWny8E6Qkrze53MR7OuAmmNJ1m1nN8SxDrG6a08J0-Fbas5OjAQz3c17GY8mVuDPOBIOVjMEghBlgl3nOi1ysxbRGhHLEK4s0KKbeRogZdgt1DkQxDFxxn41QWDw_mmMCjs9qxg0zcZzqEJw"
  }]
}
}`
